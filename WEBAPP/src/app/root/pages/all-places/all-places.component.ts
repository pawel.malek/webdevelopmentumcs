import {Component, OnInit, ViewChild} from '@angular/core';
import {ClientService} from '../../services/client.service';
import {PlaceModel} from '../../model/PlaceModel';
import {AgmMap, MarkerManager} from '@agm/core';
import {GoogleMapsAPIWrapper} from '@agm/core/services';

@Component({
  selector: 'app-all-places',
  templateUrl: './all-places.component.html',
  styleUrls: ['./all-places.component.scss'],
  providers: [ ClientService, GoogleMapsAPIWrapper, MarkerManager ]
})
export class AllPlacesComponent implements OnInit {

  @ViewChild(AgmMap) map: AgmMap;

  lat: number = 51.24582604;
  lng: number = 22.54229844;
  markers = [];
  isLoading = false;
  public type: string = null;
  public str: string;
  types: string[] = ['Zabytek', 'Galeria', 'Park', 'Wszystko', 'Uczelnia'];

  origin: any = null;
  destination: any = null;

  originTmp: any = null;
  originName: string;

  destinationName: string;
  destinationTmp: any = null;

  setOrigin: boolean = true;
  setDestination: boolean = false;

  constructor(private readonly service: ClientService,
              private readonly googleMapsAPIWrapper: GoogleMapsAPIWrapper,
              private readonly markerManager: MarkerManager) {
  }

  ngOnInit() {
    this.markers = [];
     if (this.type === null) {
       this.loadAllPlaces();
     } else {
       this.loadPlacesByType(this.type);
     }
  }

  loadAllPlaces(): void {
    this.service.getAllPlaces().subscribe((places: PlaceModel[]) => {
      this.markers = places;
      this.isLoading = true;
      this.str = JSON.stringify(this.markers);
    });
  }

  loadPlacesByType(type: string): void {
    this.type = type;
    this.service.findByType(type).subscribe((places: PlaceModel[]) => {
      this.markers = places;
      this.isLoading = true;
      this.str = JSON.stringify(this.markers);
    });
  }

  public clickHandler(marker: PlaceModel): void {
      this.origin = null;
      this.destination = null;

      if (this.setOrigin) {
        this.originTmp = {lat: marker.latitude, lng: marker.longitude};
        this.setOrigin = false;
        this.originName = marker.name;
        this.setDestination = true;

      } else {
        if (this.setDestination) {
          this.destinationTmp  = {lat: marker.latitude, lng: marker.longitude};
          this.destinationName = marker.name;
          this.setDestination = false;

        } else {
          this.setOrigin = true;
          this.clickHandler(marker);
        }
      }
  }

  public generatePath(): void {
    this.origin = this.originTmp;
    this.destination = this.destinationTmp;
  }

  public onChange(value): void {
      if (value === 'Wszystko') {
        this.loadAllPlaces();
      } else {
        this.loadPlacesByType(value);
      }
  }
}
