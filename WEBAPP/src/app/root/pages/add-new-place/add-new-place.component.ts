import {Component, OnInit} from '@angular/core';
import {PlaceModel} from '../../model/PlaceModel';
import {ClientService} from '../../services/client.service';
import {GoogleMapsAPIWrapper, MarkerManager} from '@agm/core';
import {Message} from '../../model/Message';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-new-place',
  templateUrl: './add-new-place.component.html',
  styleUrls: ['./add-new-place.component.scss'],
  providers: [ ClientService, GoogleMapsAPIWrapper, MarkerManager ]
})
export class AddNewPlaceComponent implements OnInit {

  types: string[] = ['Zabytek', 'Galeria', 'Uczelnia', 'Park', 'Inne'];
  model: PlaceModel;
  message: Message;
  addForm: FormGroup;
  submit: boolean = false;

  constructor(private readonly service: ClientService, private formBuilder: FormBuilder) {
     this.model = new PlaceModel();
     this.message = new Message();
  }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: ['',Validators.required],
      description: [''],
      latitude: ['', Validators.required, Validators.min(0), Validators.max(180)],
      longitude: ['', Validators.required, Validators.min(0), Validators.max(180)],
      type: ['',Validators.required]
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    this.submit = true;
    if(this.addForm.invalid === true){
      return;
    }
    try{

      this.model = this.addForm.value;
      this.service.addNewPlace(this.model);
      this.message.message = "Dodano miejsce!";
      this.message.isInfo = true;
    } catch(err) {
      this.message.message = "Nie udało się dodać miejsca.";
      this.message.isInfo = false;
    }
  }

}
