import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { AgmDirectionModule } from 'agm-direction';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule, GoogleMapsAPIWrapper, MarkerManager } from '@agm/core';
import { NavbarComponent } from './layout-components/navbar/navbar.component';
import { HomeImgCarouselComponent } from './layout-components/home-img-carousel/home-img-carousel.component';
import { HomeComponent } from './pages/home/home.component';
import { AllPlacesComponent } from './pages/all-places/all-places.component';
import { AddNewPlaceComponent } from './pages/add-new-place/add-new-place.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent, NavbarComponent, HomeImgCarouselComponent, HomeComponent, AllPlacesComponent, AddNewPlaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDYaXBe_dfbMwu6ENqlJSqBATwc4DMKGTI'
    }),
    AgmDirectionModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
