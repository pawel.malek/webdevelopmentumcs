import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllPlacesComponent } from './pages/all-places/all-places.component';
import { HomeComponent } from './pages/home/home.component';
import { AddNewPlaceComponent } from './pages/add-new-place/add-new-place.component';

// const routes: Routes = [];
const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'all-places', component: AllPlacesComponent },
  { path: 'add-new', component: AddNewPlaceComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
