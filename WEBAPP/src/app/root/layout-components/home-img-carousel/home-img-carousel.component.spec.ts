import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeImgCarouselComponent } from './home-img-carousel.component';

describe('HomeImgCarouselComponent', () => {
  let component: HomeImgCarouselComponent;
  let fixture: ComponentFixture<HomeImgCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeImgCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeImgCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
