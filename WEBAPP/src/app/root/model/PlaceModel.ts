export class PlaceModel {

  public id: number;
  public name: string;
  public description: string;
  public latitude: number;
  public longitude: number;
  public type: string;
  public image?: string;

  public setAllFields(name, desc, latitude, longitude, type): void {
      this.name = name;
      this.description = desc;
      this.latitude = latitude;
      this.longitude = longitude;
      this.type = type;
  }
}
