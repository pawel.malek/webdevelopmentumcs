import {HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class HttpConfig {

  private _endpoint = 'http://localhost:3000/locations/';

  private _httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  get endpoint(): string {
    return this._endpoint;
  }

  set endpoint(value: string) {
    this._endpoint = value;
  }

  get httpOptions(): { headers: HttpHeaders } {
    return this._httpOptions;
  }

  set httpOptions(value: { headers: HttpHeaders }) {
    this._httpOptions = value;
  }
}
