import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {PlaceModel} from '../model/PlaceModel';
import {Observable, throwError} from 'rxjs';
import {HttpConfig} from './http.config';

@Injectable()
export class ClientService  {

  private config: HttpConfig = new HttpConfig();

  constructor(private http: HttpClient) {}

  getAllPlaces(): Observable<PlaceModel[]> {
     return this.http.get<PlaceModel[]>(this.config.endpoint + 'findAll/', this.config.httpOptions);
  }

  addNewPlace(body: PlaceModel) {
     return this.http.post<any>(this.config.endpoint + 'create/', JSON.stringify(body), this.config.httpOptions).subscribe();
  }

  findByType(type: string): Observable<PlaceModel[]> {
     return this.http.get<PlaceModel[]>(this.config.endpoint + 'findByType/' + type, this.config.httpOptions);
  }
}



