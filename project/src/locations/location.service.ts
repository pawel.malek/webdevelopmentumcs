import {Injectable} from '@nestjs/common';
import {Location} from './interface/location.interface';
import {LocationDto} from './dto/location.dto';

@Injectable()
export class LocationsService {
    private readonly locations: Location[] = [

        {id: 1, name: 'Stare Miasto', description: 'Stare Miasto', type: 'Zabytek', latitude: 51.24800198, longitude: 22.57025242, image: 'zabytek.png'},
        {id: 2, name: 'Ogrod Saski', description: 'Ogrod Saski', type: 'Park', latitude: 51.24939883, longitude: 22.54630566,  image: 'ogrod.png'},
        {id: 3, name: 'UMCS INFORMATYKA', description: 'desc3', type: 'Uczelnia', latitude: 51.24582604, longitude: 22.54229844, image: 'uczelnia.png'},
        {id: 4, name: 'Plaza', description: 'Stare Miasto', type: 'Galeria', latitude: 51.2482423, longitude: 22.5248482, image: 'uczelnia.png'},
        {id: 5, name: 'Zamek Lubelski', description: 'Ogrod Saski', type: 'Zabytek', latitude: 51.250487, longitude: 22.572208,  image: 'zabytek.png'},
        {id: 6, name: 'Pomnik Uni lubelskiej', description: 'desc3', type: 'Zabytek', latitude: 51.2484299, longitude: 22.5532645, image: 'zabytek.png'},
        {id: 7, name: 'Centrum spotkan kultur', description: 'Stare Miasto', type: 'Galeria', latitude: 51.2486411, longitude: 22.5524795, image: 'uczelnia.png'},
        {id: 8, name: 'Inkubator Technologiczny w Lublinie', description: 'Ogrod Saski', type: 'Uczelnia', latitude: 51.2497489, longitude: 22.5760836,  image: 'uczelnia.png'},
        {id: 9, name: 'Atrium Felicity', description: 'desc3', type: 'Galeria', latitude: 51.2320598, longitude: 22.5775851, image: 'galeria.png'},
        {id: 10, name: 'Państwowe Muzeum na Majdanku', description: 'desc3', type: 'Zabytek', latitude: 51.223632, longitude: 22.609106, image: 'zabytek.png'}
    ];


    private index: number = 11;
    create(location: LocationDto) {

        this.locations.push({
            id: this.index++,
            name: location.name,
            description: location.description,
            type: location.type,
            latitude: location.latitude,
            longitude: location.longitude,
            image: location.image || "zabytek.png",
        }) ;
    }
    findAll(): Location[] {
        return this.locations;
    }

    findById(id: number): Location {
        return this.locations.find(x => x.id === id);
    }

    findByType(type: string): Location[] {
        return this.locations.filter(x => x.type === type);
    }

    update(id: number, location: LocationDto): Location {
        console.log(id);
        let index = this.locations.findIndex(x => x.id ===id);
        console.log('index: '+ index);
        if (index >=0) {
            this.locations[index].description = location.description;
            this.locations[index].type = location.type;
            this.locations[index].latitude = location.latitude;
            this.locations[index].longitude = location.longitude;
            this.locations[index].name = location.name;
            return this.locations[index];
        }
        return null;
    }

    delete(id: number) {
        let toDelete = this.findById(id);
        if (toDelete !== null) {
            let indexToDel = this.locations.findIndex(x => x.id == id);
            this.locations.splice(indexToDel, 1);
        }
    }
}
