import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {LocationsService} from './location.service';
import {LocationDto} from './dto/location.dto';
import {Location} from './interface/location.interface';
import {ParseIntPipe} from '../common/parse-int.pipe';

@Controller('locations')
export class LocationsController {
    constructor(private readonly service: LocationsService) { }

    @Post('/create/')
    async create(@Body() locationDto: LocationDto) {
        return this.service.create(locationDto);
    }

    @Get('/findAll/')
    async findAll(): Promise<Location[]> {
        return this.service.findAll();
    }

    @Get('/findByType/:type')
    async findByType(
        @Param('type') type): Promise<Location[]> {
        return this.service.findByType(type);
    }

    @Get('/findById/:id')
    async findOne(
        @Param('id', new ParseIntPipe()) id): Promise<Location> {
        return this.service.findById(id);
    }

    @Delete('/delete/:id')
    async delete(
        @Param('id', new ParseIntPipe()) id) {
        this.service.delete(id);
    }

    @Put('/update/:id')
    async updateLocation(
        @Param('id', new ParseIntPipe()) id, @Body() locationDto: LocationDto
    ): Promise<Location> {
        return this.service.update(id, locationDto);
    }
}
