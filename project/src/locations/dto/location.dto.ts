import {IsNumber, IsString} from 'class-validator';

export class LocationDto {
    @IsString() name: string;

    @IsString() description: string;

    @IsString() type: string;

    @IsNumber() latitude: number;

    @IsNumber() longitude: number;

    @IsString() image: string;

}
