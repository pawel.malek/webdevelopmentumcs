import { Module } from '@nestjs/common';
import { LocationsModule } from './locations/location.module';

@Module({
 imports: [LocationsModule],
})
export class AppModule {}
